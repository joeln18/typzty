const Colors = {
    gray: '#f5f6fa',
    primary: '#6614f5',
    grayDark: '#3b4859'
}

export default Colors;