import { useState, useEffect } from 'react';


const useUser = ( limit, initialState = [] ) => {
    const [data, setData] = useState(initialState);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(`https://dummyapi.io/data/api/user/?page=1&limit=${limit}`,{
            headers: {
                Accept: "*/*",
                'app-id': "60736706345b03b416a75d21"
            },
              method: "GET",
              
        })
        .then(res => res.json())
        .then(resJson => {
            setData(resJson);
            setIsLoading(false);
        });

    }, [limit]);

    return [ data, isLoading ]
}

export default useUser;