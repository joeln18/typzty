import React from 'react';

const Image = (component) => (
    <img src={component} alt='' />
);

export default Image;