import { Layout, Row, Col, Typography, Input } from 'antd';
import Icon, { MenuOutlined, CaretDownOutlined, PlusOutlined } from '@ant-design/icons';
import IconStore from '../../assets/img/icons/store_icon.svg';
import IconSearch from '../../assets/img/icons/search_icon.svg';
import IconMarker from '../../assets/img/icons/marker_icon.svg';
import IconReviews from '../../assets/img/icons/tv_reviews.svg';
import IconCalendar from '../../assets/img/icons/calendar_icon.svg';
import Colors from '../../themes/Colors' ;
import Image from './Image';


// import '../../assets/css/Navbar.css';
// import './navBar.less';


const { Header } = Layout;
const { Title } = Typography;



const NavBar = () => {

    return(
        <div>
            <Header style={{backgroundColor: 'white', flexDirection: 'row'}}>
              <Row>
                <Col xs={24} md={24} lg={10}>
                  <Row style={{ paddingTop: 15 }}>
                      <MenuOutlined style={{fontSize: 25, marginRight: 5}} />
                      <Title level={5}>TonyMolyColombia</Title>
                      <CaretDownOutlined style={{fontSize: 15, marginLeft: 10, marginTop: 8}} />
                  </Row>
                </Col>
                <Col xs={24} md={12} lg={9} style={{paddingTop: 15}} className="headerBar">
                  <Input 
                    size='large' 
                    placeholder='Busca reviews, influentes, marcas y productos' 
                    addonAfter={<Icon component={()=>Image(IconSearch)} style={{ fontSize: 25 }} />} 
                  />
                </Col>
                <Col xs={24} md={12} lg={5} style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 15 }}>
                  <Row>
                    <PlusOutlined style={{ fontSize: 20, backgroundColor: Colors.gray, padding: 5, borderRadius: 90 }} />
                    <Icon component={()=>Image(IconStore)} style={{fontSize: 31, paddingTop: 8, marginLeft: 20 }} />
                    <Icon component={()=>Image(IconMarker)} style={{fontSize: 31, paddingTop: 8, marginLeft: 20 }} />
                    <Icon component={()=>Image(IconReviews)} style={{fontSize: 31, paddingTop: 8, marginLeft: 20 }} />
                    <Icon component={()=>Image(IconCalendar)} style={{fontSize: 31, paddingTop: 8, marginLeft: 20 }} />
                  </Row>
                </Col>
              </Row>                  
            </Header>
        </div>
    )
}

export default NavBar;
