import React from 'react';
import { Typography, Divider, Row, Col, Image } from 'antd';

import Marker from '../../../assets/img/icons/marker_icon.svg';
import Reviews from '../../../assets/img/icons/tv_reviews.svg';
import Calendar from '../../../assets/img/icons/calendar_icon.svg';
import Store from '../../../assets/img/icons/store_icon.svg';

const { Title, Text, Paragraph } = Typography;

const Overview = () => {


    return(
        <div style={{backgroundColor: 'white', padding: 10}}>
            <Title level={4}>Descripción</Title>
            <Paragraph disabled style={{fontSize: 11}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </Paragraph>
            <Divider />
            <Row>
                <Title level={4} style={{marginBottom: 20}}>Sobre la marca</Title>
            </Row>
            <Col>
                
                <Row style={{marginBottom: 15}}>
                    <Col xs={12}>
                        <Row>
                            <Image 
                                src={Store}
                            />
                            <Title level={5} style={{marginLeft: 10}}>Tipo de empresa</Title>
                        </Row>
                    </Col>
                    <Col xs={12}>
                        <Text disabled style={{fontSize: 11, fontWeight: 'bold'}}>Distribuidora internacional verificada</Text>
                    </Col>
                </Row>
                    
                <Row style={{marginBottom: 15}}>
                    <Col xs={12}>
                        <Row>
                            <Image 
                                src={Marker}
                                
                            />
                            <Title level={5} style={{marginLeft: 10}}>Sucursales en</Title>
                        </Row>
                    </Col>
                    <Col xs={12}>
                        <Text disabled style={{fontSize: 11, fontWeight: 'bold'}}>Medellín, Bogotá, Cali</Text>
                    </Col>
                </Row>

                <Row style={{marginBottom: 15}}>
                    <Col xs={12}>
                        <Row>
                            <Image 
                                src={Reviews}
                            
                            />
                            <Title level={5} style={{marginLeft: 10}}>Reviews</Title>
                        </Row>
                    </Col>
                    <Col xs={12}>
                        <Text disabled style={{fontSize: 11, fontWeight: 'bold'}}>Más de 326 Reviews de 326 usuarios</Text>
                    </Col>
                </Row>

                <Row style={{marginBottom: 15}}>
                    <Col xs={12}>
                        <Row>
                            <Image 
                                src={Calendar}
                            />
                            <Title level={5} style={{marginLeft: 10}}>En Tipzty desde</Title>
                        </Row>
                    </Col>
                    <Col xs={12}>
                        <Text disabled style={{fontSize: 11, fontWeight: 'bold'}}>El 6 de diciembre 2019</Text>
                    </Col>
                </Row>
                
            </Col>
            <Row>
            
            </Row>
        </div>
    )
}

export default Overview;