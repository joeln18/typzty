import React, { useState } from 'react';
import { Row, Col, Typography, Image, Space, Button } from 'antd';

import Icon from '@ant-design/icons';
import ImageBanner from '../../assets/img/pexels-anderson-guerra-1115128.jpg';
import ImageLogo from '../../assets/img/icons/logo.jpg';
import ImageOverview from '../../assets/img/icons/overview.png';
import ImageProducto from '../../assets/img/icons/producto.png';
import ImageReview from '../../assets/img/icons/reviews.png';
import IconVerify from '../../assets/img/icons/brand_verify_icon.svg';
import ImageM from '../../shared/components/Image';

import Overview from './components/Overview';

import Colors from '../../themes/Colors';

/**
 * 
 * https://images.pexels.com/photos/1115128/pexels-photo-1115128.jpeg?cs=srgb&dl=pexels-anderson-guerra-1115128.jpg&fm=jpg
*/

const { Paragraph, Title, Text } = Typography;

const Activity = () => {
    const [selectOption, setSelectOption] = useState({producto: false, review: false, overview: true});

    

    return(
        <>
            <Row>
                <Col xs={24}>
                    <Image 
                        width='100%'
                        src={ImageBanner}
                        
                    />
                    <div style={{
                        backgroundColor: 'white', 
                        borderTopLeftRadius: 12, 
                        borderTopRightRadius: 8, 
                        position: 'relative', 
                        marginTop: -70, 
                        paddingLeft: 20,
                        paddingRight: 20,
                        paddingBottom: 20
                        }}>
                        <Row style={{paddingTop: 10}}>
                            <Col lg={4} xs={6} sm={6}>
                                <Image
                                    width= '70%'
                                    src={ImageLogo}
                                    
                                />
                            </Col>
                            <Col lg={11} xs={18} sm={18} >
                                <Space direction='horizontal'>
                                    <Text strong style={{fontSize: 12}}>Tonymoly Colombia</Text>
                                    <Icon component={()=>ImageM(IconVerify)} style={{marginBottom: 5}} />
                                </Space>
                                <Text disabled style={{fontSize: 10}}>@tonymoly.colombia</Text>
                            </Col>
                            <Col lg={3} xs={8} sm={8}>
                                <Title level={5} style={{ marginBottom: 0, textAlign: 'center'}}>120</Title>
                                <Title level={5} style={{ textAlign: 'center', fontSize: 8, marginTop: 0}}>SEGUIDORES</Title>
                            </Col>
                            <Col lg={3} xs={8} sm={8}>
                                <Title level={5} style={{ marginBottom: 0, textAlign: 'center'}}>120</Title>
                                <Title level={5} style={{ textAlign: 'center', fontSize: 8, marginTop: 0}}>REVIEWS</Title>
                            </Col>
                            <Col lg={3} xs={8} sm={8}>
                                <Title level={5} style={{ marginBottom: 0, textAlign: 'center'}}>120</Title>
                                <Title level={5} style={{ textAlign: 'center', fontSize: 8, marginTop: 0}}>PRODUCTOS</Title>
                            </Col>
                        </Row>
                        <Row>
                            <Paragraph disabled style={{ paddingTop: 20, fontSize: 12}}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                            </Paragraph>
                        </Row>
                        <Row>
                            <Button type="primary" 
                                style={{ 
                                    borderRadius: 4, 
                                    marginRight: 15, 
                                    backgroundColor: Colors.primary, 
                                    borderColor: Colors.primary,
                                    fontSize: 11,
                                    fontWeight: 'bold'
                                }}
                                >EDITAR PERFIL
                            </Button>
                            <Button
                                style={{ 
                                    borderRadius: 4, 
                                    fontSize: 11,
                                    fontWeight: 'bold'
                                }}
                            >
                                COMPARTIR TIENDA
                            </Button>
                        </Row>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={8} 
                    style={{textAlign: 'center', cursor: 'pointer'}}
                    onClick={()=>setSelectOption({overview: false, review: false, producto: true})}
                >
                    <Space direction='horizontal' style={{paddingTop: 30, paddingBottom: 30}}>
                        <Image
                            src={ImageProducto}
                            style={{color: 'blue'}}
                        />
                        <Text style={{color: Colors.grayDark, fontSize: 16}}>Productos</Text>
                    </Space>
                </Col>
                <Col xs={8} 
                    style={{textAlign: 'center', cursor: 'pointer'}}
                    onClick={()=>setSelectOption({overview: false, review: true, producto: false})}
                >
                    <Space direction='horizontal' style={{paddingTop: 30, paddingBottom: 30}}>
                        <Image
                            src={ImageReview}
                        />
                        <Text style={{color: Colors.grayDark, fontSize: 16}}>Reviews</Text>
                    </Space>
                </Col>
                <Col xs={8} 
                    style={{textAlign: 'center', cursor: 'pointer'}} 
                    onClick={()=>setSelectOption({overview: true, review: false, producto: false})}
                >
                    <Space direction='horizontal' style={{paddingTop: 30, paddingBottom: 30}}>
                        <Image
                            src={ImageOverview}
                        />
                        <Text style={{color: Colors.grayDark, fontSize: 16 }}>Overview</Text>
                    </Space>
                </Col>
            </Row> 
            <Row>
                <Col xs={24}>
                    {
                        selectOption.overview && (
                            <Overview />
                        )
                    }
                </Col>
            </Row> 
        </>
    )
}

export default Activity;