import React from 'react';
import { Row, Col } from 'antd';
import NavBar from '../shared/components/Navbar';
import Sidebar from './sidebar/Sidebar';
import Activity from './activity/Activity';

import Colors from '../themes/Colors';

import 'antd/dist/antd.css';

const Home = () => {

    return(
        <>
            <Row>
               <Col xs={24}>
                   <NavBar />
                   <Row style={{marginTop: 20, backgroundColor: Colors.gray}}>
                       <Col xs={8} sm={8} md={8} lg={8} style={{paddingRight: 30}}>
                            <Sidebar />
                       </Col>
                       <Col xs={8} sm={8} md={8} lg={8}>
                           <Activity />
                       </Col>
                       
                   </Row>
               </Col>
               
            </Row>
        </>
    )
}

export default Home;