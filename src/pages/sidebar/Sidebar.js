import React, { useState } from 'react';
import useUser from '../../services/useUser';

import { Row, Col, Input, Typography, Spin } from 'antd';
import Icon, { RightOutlined, DownOutlined } from '@ant-design/icons';
import styled from 'styled-components';

import Image from '../../shared/components/Image';
import ItemMenu from './components/ItemMenu';
import User from './components/ContainUser';


import IconSearch from '../../assets/img/icons/search_icon.svg';
import IconCompas from '../../assets/img/icons/compass_icon.svg';
import IconStars from '../../assets/img/icons/starts_icon.svg';



import Colors from '../../themes/Colors';

const { Title } = Typography;

const StyledSpin = styled(Spin)`
    margin: 20px 0;
    margin-bottom: 20px;
    padding: 30px 50px;
    text-align: center;
    border-radius: 4px;
`


const Sidebar = () => {
    const [limit, setLimit] = useState(5);
    const [ users, isLoading ] = useUser( limit, {} );
    const [isLimit, setIsLimit] = useState(false);
    
    const seeAll = () => {
        setLimit(30);
        setIsLimit(true);
    }

    return(
        <Row style={{justifyContent: 'end', marginTop: 10}}>
            <Col xs={12} sm={12} md={12} lg={12}>
                <Input 
                    size='middle' 
                    placeholder='Busca tus productos' 
                    addonAfter={<Icon component={()=>Image(IconSearch)} style={{ fontSize: 25 }} />} 
                />
                <Row style={{ marginTop: 20, alignItems: 'center', justifyContent: 'space-between' }}>
                    <Icon component={()=>Image(IconCompas)} style={{height: 30}} />
                    <Title level={4} style={{color: Colors.primary}}>Explora todo</Title>
                    <RightOutlined style={{ fontSize: 16, alignSelf: 'center', marginBottom: 3, color: Colors.primary, right: 0 }} />
                </Row>
                <ItemMenu image={IconStars} color={Colors.grayDark} title='Categorias' />
                <ItemMenu image={IconStars} color={Colors.grayDark} title='Ofertas' />
                <ItemMenu image={IconStars} color={Colors.grayDark} title='Lo nuevo' />
                <Title level={3} style={{color: Colors.grayDark, marginTop: 30}}>Tus Creadores</Title>
                {
                    (!isLoading) ? (
                        users.data.map((element, i )=> (
                            <User url={element.picture} name={`${element.firstName} ${element.lastName}`} user={element.email}  />
                        ))
                    ):(
                        <StyledSpin />
                    )
                }
                {
                    !isLimit && (
                        <Row style={{alignItems: 'center', cursor: 'pointer', marginTop: 10}} onClick={()=>seeAll()}>
                            <Title level={4} style={{color: Colors.primary, marginRight: 10}}>Ver todo</Title>
                            <DownOutlined style={{color: Colors.primary}} />
                        </Row>
                    )
                }
                
                
            </Col>

        </Row>
    )
}

export default Sidebar;