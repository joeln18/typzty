import React from 'react';
import { Row, Col, Typography } from 'antd';
import Icon from '@ant-design/icons';
import Image from '../../../shared/components/Image';
import IconVerify from '../../../assets/img/icons/brand_verify_icon.svg';

import Colors from '../../../themes/Colors';

const { Title, Text } = Typography;

const ContainUser = ({url, name, user}) => {

    const username = user.split('@')[0];

    return(
        <Row style={{marginTop: 30}}>
            <Col xs={24} sm={24} md={24} lg={6}>
                <img
                    src={url}
                    style={{borderRadius: 90, height: 45}}
                    className="imgAvatar"
                    alt={name}
                    title={name}
                />
            </Col>
            <Col xs={24} sm={24} md={24} lg={18} >
                <Row>
                    <Title level={5} style={{color: Colors.grayDark}}>{ name }</Title>
                    <Icon component={()=>Image(IconVerify)} style={{ width: 5, alignItems: 'center', marginLeft: 8, marginTop: 3}} />
                </Row>
                <Text disabled>@{username}</Text>
            </Col>
        </Row>
    )
}

export default ContainUser;