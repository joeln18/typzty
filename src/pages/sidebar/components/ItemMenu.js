import React from 'react';
import { Row, Typography } from 'antd';
import Icon, { RightOutlined } from '@ant-design/icons';
import Image from '../../../shared/components/Image';


const { Title } = Typography;


const ItemMenu = ({image, color, title}) => {
    return(
        <Row style={{ marginTop: 20, alignItems: 'center', justifyContent: 'space-between' }}>
            <Icon component={()=>Image(image)} style={{height: 30}} />
            <Title level={4} style={{color}}>{title}</Title>
            <RightOutlined style={{ fontSize: 16, alignSelf: 'center', marginBottom: 3, color, right: 0 }} />
        </Row>
    )
}

export default ItemMenu;